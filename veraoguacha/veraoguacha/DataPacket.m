//
//  DataPacket.m
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 08/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import "DataPacket.h"

@implementation DataPacket

@synthesize accelerometer,attitude,gravity,gyroscope,magnetometer,orientation;

@end
