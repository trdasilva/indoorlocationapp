//
//  ViewController.h
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 07/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCDAsyncSocket.h"
#import "DataPacket.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <UIAlertViewDelegate,CLLocationManagerDelegate>

@property(nonatomic,strong) GCDAsyncSocket *asyncSocket;
@property(nonatomic,strong) NSString *host;
@property(nonatomic,assign) uint16_t port;
@property(nonatomic,assign) BOOL wasSendingData;
@property(nonatomic,assign) BOOL shuoldSendData;
@property(nonatomic,assign) BOOL shouldConnectToServer;
@property(nonatomic,assign) BOOL wasConnectedToServer;

@property(nonatomic,assign) IBOutlet UILabel *lblIp;
@property(nonatomic,assign) IBOutlet UILabel *lblPort;
@property(nonatomic,assign) IBOutlet UITextField *txtIP;
@property(nonatomic,assign) IBOutlet UITextField *txtPort;

@property(nonatomic,strong) Vector3D *magnetometer;

@property(nonatomic,strong) CLLocationManager *locationManager;

@property(nonatomic,assign) IBOutlet UIButton *startStopSendDataButton;
@property(nonatomic,assign) IBOutlet UIButton *connectToServer;

-(IBAction)ditTapStartStopSendDataButton:(id)sender;
-(IBAction)ditTapConnectToServer:(id)sender;
-(IBAction)didTapOnTheView:(id)sender;

@end
