//
//  main.m
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 07/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
