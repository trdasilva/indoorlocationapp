//
//  Vector3D.h
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 08/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface Vector3D : JSONModel

@property (assign, nonatomic) double x;
@property (assign, nonatomic) double y;
@property (assign, nonatomic) double z;

@end
