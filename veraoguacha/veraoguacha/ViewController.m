//
//  ViewController.m
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 07/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
{
}

@end

@implementation ViewController

@synthesize asyncSocket,host,port,shuoldSendData,wasSendingData,
startStopSendDataButton,wasConnectedToServer,magnetometer,
locationManager,shouldConnectToServer;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(didTapOnTheView:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    self.startStopSendDataButton.enabled = NO;
    
    magnetometer = [[Vector3D alloc] init];
    
    // setup the location manager
	self.locationManager = [[CLLocationManager alloc] init];
    // heading service configuration
    locationManager.headingFilter = kCLHeadingFilterNone;
    
    // setup delegate callbacks
    locationManager.delegate = self;
    
    // start the compass
    [locationManager startUpdatingHeading];
    
    wasConnectedToServer = NO;
    
    asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    host = @"192.168.0.21";
	port = 60000;
    
    shuoldSendData = NO;
    wasSendingData = NO;
    shouldConnectToServer = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
    [self startUpdates];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [self stopUpdates];
}

#pragma  mark Custom Methods
- (void)startUpdates
{
    NSTimeInterval updateInterval = 1/60;
    
    CMMotionManager *mManager = [(AppDelegate *)[[UIApplication sharedApplication] delegate] sharedManager];
    
    
    if ([mManager isDeviceMotionAvailable] == YES) {
        
        [mManager setDeviceMotionUpdateInterval:updateInterval];
    
        [mManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion *deviceMotion, NSError *error) {
            
            DataPacket *packet = [[DataPacket alloc] init];
            
            // attitude
            Vector3D *attitude = [[Vector3D alloc] init];
            attitude.x = deviceMotion.attitude.roll;
            attitude.y = deviceMotion.attitude.pitch;
            attitude.z = deviceMotion.attitude.yaw;
            
            //rotationRate
            Vector3D *gyroscope = [[Vector3D alloc] init];
            gyroscope.x = deviceMotion.rotationRate.x;
            gyroscope.y = deviceMotion.rotationRate.y;
            gyroscope.z = deviceMotion.rotationRate.z;
            
            // gravity
            Vector3D *gravity = [[Vector3D alloc] init];
            gravity.x = deviceMotion.gravity.x;
            gravity.y = deviceMotion.gravity.y;
            gravity.z = deviceMotion.gravity.z;
            
            // userAcceleration
            Vector3D *accelerometer = [[Vector3D alloc] init];
            accelerometer.x = deviceMotion.userAcceleration.x;
            accelerometer.y = deviceMotion.userAcceleration.y;
            accelerometer.z = deviceMotion.userAcceleration.z;
            
            //Device orientation
            Quaternion *orientation = [[Quaternion alloc] init];
            orientation.x = deviceMotion.attitude.quaternion.x;
            orientation.y = deviceMotion.attitude.quaternion.y;
            orientation.z = deviceMotion.attitude.quaternion.z;
            orientation.w = deviceMotion.attitude.quaternion.w;
            
            packet.accelerometer = accelerometer;
            packet.gravity = gravity;
            packet.gyroscope = gyroscope;
            packet.attitude = attitude;
            packet.magnetometer = magnetometer;
            packet.orientation = orientation;
            
            if ([asyncSocket isConnected]) {
                
                if(shuoldSendData)
                {
                    NSData *msg = [ [NSString stringWithFormat:@"%@\n",[packet toJSONString]] dataUsingEncoding:NSUTF8StringEncoding];
                    
                    [asyncSocket writeData:msg withTimeout:0.1 tag:0];
                }
                else if (wasSendingData)
                {
                    
                    wasSendingData = NO;
                    
                    NSData *msg = [ @"exit\n" dataUsingEncoding:NSUTF8StringEncoding];
                    
                    [asyncSocket writeData:msg withTimeout:0.1 tag:0];
                    
                }
            }
            else if(shouldConnectToServer)
            {
                if (![asyncSocket connectToHost:host onPort:port error:&error])
                {
                    
                }
            }
            
        }];
    }
    
}

// This delegate method is invoked when the location manager has heading data.
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)heading {
    // Update the labels with the raw x, y, and z values.
    magnetometer.x = heading.x;
	magnetometer.y = heading.y;
	magnetometer.z = heading.z;
}

- (void)stopUpdates
{
    CMMotionManager *mManager = [(AppDelegate *)[[UIApplication sharedApplication] delegate] sharedManager];
    
    if ([mManager isDeviceMotionActive] == YES) {
        [mManager stopDeviceMotionUpdates];
    }
}

-(IBAction)ditTapStartStopSendDataButton:(id)sender
{
    if (shuoldSendData)
    {
        shuoldSendData = NO;
        [startStopSendDataButton setTitle:@"Start Sending Data" forState:UIControlStateNormal];
        
    }
    else
    {
        shuoldSendData = YES;
        wasSendingData = YES;
        [startStopSendDataButton setTitle:@"Stop Sending Data" forState:UIControlStateNormal];
    }
}

-(IBAction)didTapOnTheView:(id)sender
{
    [self.view endEditing:YES];
}

-(IBAction)ditTapConnectToServer:(id)sender
{
    NSError *error = nil;
	
    if([asyncSocket isConnected])
    {
        shouldConnectToServer = NO;
        [asyncSocket disconnect];
        [self.connectToServer setTitle:@"Connect to Server" forState:UIControlStateNormal];
        self.shuoldSendData = NO;
        [self.startStopSendDataButton setTitle:@"Start Sending Data" forState:UIControlStateNormal];
        self.startStopSendDataButton.enabled =NO;
    }
    else
    {
        shouldConnectToServer = YES;
        host = self.txtIP.text;
        port = [self.txtPort.text integerValue];
        
        if (![asyncSocket connectToHost:host onPort:port error:&error])
        {
            
        }
    }
}

#pragma mark AsynckSocketDelegate
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
	[self.connectToServer setTitle:@"Disonnect from Server" forState:UIControlStateNormal];
    self.shuoldSendData = NO;
    [self.startStopSendDataButton setTitle:@"Start Sending Data" forState:UIControlStateNormal];
    self.startStopSendDataButton.enabled =YES;
    
    wasConnectedToServer = YES;
}

- (void)socketDidSecure:(GCDAsyncSocket *)sock
{
	// This method will be called if USE_SECURE_CONNECTION is set
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{

}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
	
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    if(wasConnectedToServer)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Disconnected from server" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        
        wasConnectedToServer = NO;
        shouldConnectToServer = NO;
        shuoldSendData = NO;
        [self.startStopSendDataButton setTitle:@"Start Sending Data" forState:UIControlStateNormal];
        self.startStopSendDataButton.enabled = NO;
        [self.connectToServer setTitle:@"Connect to Server" forState:UIControlStateNormal];
        
        
    }
}


@end
