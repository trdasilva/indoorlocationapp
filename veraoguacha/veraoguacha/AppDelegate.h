//
//  AppDelegate.h
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 07/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readonly) CMMotionManager *sharedManager;


@end
