//
//  DataPacket.h
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 08/03/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import "JSONModel.h"
#import "Vector3D.h"
#import "Quaternion.h"

@interface DataPacket : JSONModel

@property (strong, nonatomic) Vector3D* accelerometer;
@property (strong, nonatomic) Vector3D* gyroscope;
@property (strong, nonatomic) Vector3D* gravity;
@property (strong, nonatomic) Vector3D* attitude;
@property (strong, nonatomic) Vector3D* magnetometer;
@property (strong, nonatomic) Quaternion *orientation;

@end
