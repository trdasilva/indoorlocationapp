//
//  Quaternion.h
//  veraoguacha
//
//  Created by Tomaz Rocha Silva on 20/05/14.
//  Copyright (c) 2014 Tomaz Rocha Silva. All rights reserved.
//

#import "JSONModel.h"

@interface Quaternion : JSONModel


@property (assign, nonatomic) double x;
@property (assign, nonatomic) double y;
@property (assign, nonatomic) double z;
@property (assign, nonatomic) double w;

@end
